package controllers;

import com.google.inject.Inject;
import play.db.Database;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import repositories.ClienteRepository;
import models.Cliente;

public class ClienteController extends Controller {
	
	@Inject public ClienteRepository clienteRepository;
    @Inject Database database;

    public Result findAll(Optional<Integer> pagina,Optional<String> dni,Optional<String> nombre,Optional<String> apellido,Optional<String> telefono,Optional<String> email) {
        return database.withConnection(conn -> {
			String where = "1=1";
			if (dni.isPresent()) {
				where = where + " and dni='"+dni.get()+"'";
			}
			if (nombre.isPresent()) {
				where = where + " and nombre='"+nombre.get()+"'";
			}
			if (apellido.isPresent()) {
				where = where + " and apellido='"+apellido.get()+"'";
			}
			if (telefono.isPresent()) {
				where = where + " and telefono='"+telefono.get()+"'";
			}
			if (email.isPresent()) {
				where = where + " and email='"+email.get()+"'";
			}
			if (pagina.isPresent() && pagina.get()>0) {
				int pag = 1;
				if (pagina.get()>1) {
				   pag = (pagina.get() - 1)*10;
				} else if (pagina.get()==1) {
					pag--;
				}
				where = where + " limit "+pag+", 10";
			} else {
				where = where +" limit 0,10";
			}
            return ok(Json.toJson(clienteRepository.findAll(where))).as("application/json");
        });
    }

    public Result findById(String dni) {
        return database.withConnection(conn -> {
            return ok(Json.toJson(clienteRepository.findById(dni))).as("application/json");
        });
    }


    public Result create() {
        return database.withConnection(conn -> {
            Cliente clienteRequest = Json.fromJson(request().body().asJson(), Cliente.class);
			if (camposValidos(clienteRequest)==true) {
				if (clienteRequest.getEmail()!=null && !clienteRequest.getEmail().equals("")) {
					if (!isEmailValido(clienteRequest.getEmail())) {
						return status(500, "email no valido").as("application/json");
					} else {
						clienteRepository.add(clienteRequest);
						return ok(Json.toJson(clienteRequest)).as("application/json");
					}
				} else {
					clienteRepository.add(clienteRequest);
					return ok(Json.toJson(clienteRequest)).as("application/json");
				}
			} else {
				return status(500, "campos no validos").as("application/json");
			}
        });
    }

    public Result update(String dni) {
        return database.withConnection(conn -> {
            Cliente clienteRequest = Json.fromJson(request().body().asJson(), Cliente.class);
            clienteRequest.setDNI(dni);
			Optional<Cliente> cliente = clienteRepository.findById(dni);
			
			if (cliente.isPresent()) {
				if (camposValidos(clienteRequest)==true) {
					if (clienteRequest.getEmail()!=null && !clienteRequest.getEmail().equals("")) {
						if (!isEmailValido(clienteRequest.getEmail())) {
							return status(400, "email no valido").as("application/json");
						} else {
							clienteRepository.update(clienteRequest);
							return ok(Json.toJson(clienteRequest)).as("application/json");
						}
					} else {
						clienteRepository.update(clienteRequest);
						return ok(Json.toJson(clienteRequest)).as("application/json");
					}
				} else {
					return status(400, "campos no validos").as("application/json");
				}
			} else {
				return status(500, "dni no encontrado").as("application/json");
			}
        });
    }


    public Result delete(String dni) {
        return database.withConnection(conn -> {
            clienteRepository.delete(dni);
            return ok("{}").as("application/json");
        });
    }
	
	private boolean isEmailValido(String mail) {
		String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
		Pattern pattern = Pattern.compile(emailPattern);
		Matcher matcher = pattern.matcher(mail);
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean isTelefonoValido(String telf) {
		String emailPattern = "[0-9]{11}$";
		Pattern pattern = Pattern.compile(emailPattern);
		Matcher matcher = pattern.matcher(telf);
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean camposValidos(Cliente cliente) {
			boolean valido = false;
		if (cliente.getDNI()!=null && !cliente.getDNI().equals("") && cliente.getDNI().length() < 45 
			&& cliente.getNombre()!=null && !cliente.getNombre().equals("") && cliente.getNombre().length() < 45
			&& cliente.getApellido()!=null && !cliente.getApellido().equals("") && cliente.getApellido().length() < 45) {
				valido = true;
		}
		
		if (cliente.getTelefono()!=null && !cliente.getTelefono().equals("")) {
			valido = isTelefonoValido(cliente.getTelefono());
		}
		return valido;
	}
}