package repositories;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import models.Cliente;
import java.sql.*;
import java.util.*;

import play.db.*;

@Singleton
public class ClienteRepository {
	@Inject Database database;
	

    public List<Cliente> findAll(String wh) throws SQLException  {
		
		PreparedStatement preparedStatement = database.getConnection().prepareStatement("select * from cliente where "+wh);

        List<Cliente> clientes = new ArrayList<>();

        preparedStatement.execute();

        ResultSet rs = preparedStatement.getResultSet();

        while (rs.next()) {
            Cliente cliente = new Cliente();
			
            cliente.setDNI(rs.getString("dni"));
            cliente.setNombre(rs.getString("nombre"));
            cliente.setApellido(rs.getString("apellido"));
			cliente.setTelefono(rs.getString("telefono"));
			cliente.setEmail(rs.getString("email"));

            clientes.add(cliente);
        }
		
        return clientes;
    }

    public Optional<Cliente> findById(String DNI) throws SQLException{
		PreparedStatement preparedStatement = database.getConnection().prepareStatement("select * from cliente where dni = ?");

		preparedStatement.setString(1, DNI);

		preparedStatement.execute();

		ResultSet rs = preparedStatement.getResultSet();

		if (rs.next()) {
			Cliente cliente = new Cliente();

            cliente.setDNI(rs.getString("dni"));
            cliente.setNombre(rs.getString("nombre"));
            cliente.setApellido(rs.getString("apellido"));
			cliente.setTelefono(rs.getString("telefono"));
			cliente.setEmail(rs.getString("email"));


			return Optional.of(cliente);
		}

		return Optional.empty();
    }

    public void delete(String dni) throws SQLException{
        PreparedStatement preparedStatement = database.getConnection().prepareStatement("delete from cliente where dni = ?");

		preparedStatement.setString(1, dni);

		preparedStatement.execute();
    }

    public void add(Cliente cliente) throws SQLException {
		PreparedStatement preparedStatement = database.getConnection().prepareStatement("insert into cliente (dni, nombre, apellido, telefono, email) values (?, ?, ?, ?, ?)");
		preparedStatement.setString(1, cliente.getDNI());
		preparedStatement.setString(2, cliente.getNombre());
		preparedStatement.setString(3, cliente.getApellido());
		preparedStatement.setString(4, cliente.getTelefono());
		preparedStatement.setString(5, cliente.getEmail());


		preparedStatement.execute();
    }

    public void update(Cliente cliente) throws SQLException {
        PreparedStatement preparedStatement = database.getConnection().prepareStatement("update cliente set nombre = ?, apellido = ?, telefono = ?, email = ? where dni = ?");

		preparedStatement.setString(1, cliente.getNombre());
		preparedStatement.setString(2, cliente.getApellido());
		preparedStatement.setString(3, cliente.getTelefono());
		preparedStatement.setString(4, cliente.getEmail());
		preparedStatement.setString(5, cliente.getDNI());

		preparedStatement.executeUpdate();
    }

}