# Users schema

# --- !Ups

CREATE TABLE `bdapi`.`cliente` (
  `dni` VARCHAR(45) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `telefono` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`dni`));

# --- !Downs

DROP TABLE cliente;